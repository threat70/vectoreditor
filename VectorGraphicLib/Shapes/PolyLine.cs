﻿using System.Collections.Generic;

namespace VectorGraphicLib.Shapes
{
    public class PolyLine : Shape
    {
        public List<Point> Points { get; private set; }

        public PolyLine()
        {
            Points = new List<Point>();
        }

        public void AddPoint(Point point)
        {
            Points.Add(point);
        }

        public override void Draw(IDrawStrategy drawer)
        {
            drawer.Draw(this);
        }
    }
}
