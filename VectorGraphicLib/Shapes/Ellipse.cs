﻿namespace VectorGraphicLib.Shapes
{
    public class Ellipse : Line 
    {
        public Ellipse(Point startPoint, Point endPoint) 
            : base(startPoint, endPoint)
        {
            // extends Line because use the same draw parameters
        }

        public override void Draw(IDrawStrategy drawer)
        {
            drawer.Draw(this);
        }
    }
}
