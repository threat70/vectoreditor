﻿using System.Drawing;

namespace VectorGraphicLib.Shapes
{
    /// <summary>
    ///     Абстрактная фигура с общими свойствами
    /// </summary>
    public abstract class Shape
    {
        /// <summary>
        ///     Общие свойства фигуры, доступ к каждому свойство можно получать из конкректных методов
        /// </summary>
        public ShapeProperties ShapeProperties { get; set; }

        /// <summary>
        ///     Толщина кисти
        /// </summary>
        public int BrushHeight
        {
            get { return ShapeProperties.BrushHeight; }
        }

        /// <summary>
        ///     Цвет контура
        /// </summary>
        public Color PenColor
        {
            get { return ShapeProperties.PenColor; }
        }

        /// <summary>
        ///     Цвет заливки
        /// </summary>
        public Color FillColor
        {
            get { return ShapeProperties.FillColor; }
        }

        /// <summary>
        ///     Тип кисти
        /// </summary>
        public BrushType BrushType
        {
            get { return ShapeProperties.BrushType; }
        }

        /// <summary>
        ///     Отрисовка фигуры с заданной стратегией.
        ///     Используется паттерн Visitor
        /// </summary>
        /// <param name="drawer">Стратегия отрисовки</param>
        public abstract void Draw(IDrawStrategy drawer);
    }
}
