﻿namespace VectorGraphicLib.Shapes
{
    /// <summary>
    ///     Точка используемых координат
    /// </summary>
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
