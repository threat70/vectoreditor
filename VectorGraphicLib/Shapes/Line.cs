﻿namespace VectorGraphicLib.Shapes
{
    /// <summary>
    ///     Фигура: Линия
    /// </summary>
    public class Line : Shape
    {
        /// <summary>
        ///     Первая точка линии
        /// </summary>
        public Point StartPoint { get; private set; }
        /// <summary>
        ///     Вторая точка линии
        /// </summary>
        public Point EndPoint { get; private set; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="endPoint"></param>
        public Line(Point startPoint, Point endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        /// <summary>
        ///     Отрисовка линии с заданной стратегией
        /// </summary>
        /// <param name="drawer">Стратегия отрисовки</param>
        public override void Draw(IDrawStrategy drawer)
        {
            drawer.Draw(this);
        }
    }
}
