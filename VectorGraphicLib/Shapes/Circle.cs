﻿using System;

namespace VectorGraphicLib.Shapes
{
    public class Circle : Shape
    {
        public Point CenterPoint { get; private set; }
        private readonly Point _radiusPoint;

        public int Radius
        {
            get
            {
                return (int) Math.Sqrt((CenterPoint.X-_radiusPoint.X)*(CenterPoint.X-_radiusPoint.X)+(CenterPoint.Y-_radiusPoint.Y)*(CenterPoint.Y-_radiusPoint.Y))
                ;
            }
        }

        public Circle(Point centerPoint, Point radiusPoint)
        {
            CenterPoint = centerPoint;
            _radiusPoint = radiusPoint;
        }

        public override void Draw(IDrawStrategy drawer)
        {
            drawer.Draw(this);
        }
    }
}
