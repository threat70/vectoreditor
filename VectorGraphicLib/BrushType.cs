﻿namespace VectorGraphicLib
{
    /// <summary>
    ///     Доступные типы кисти
    /// </summary>
    public enum BrushType
    {
        Dotted,
        Solid
    }
}
