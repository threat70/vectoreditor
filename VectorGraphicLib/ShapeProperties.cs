﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace VectorGraphicLib
{
    public class ShapeProperties : ICloneable
    {
        /// <summary>
        ///     Толщина линии
        /// </summary>
        [DisplayName(@"Толщина линии")]
        [Category("Свойства фигуры")]
        public int BrushHeight { get; set; }
        /// <summary>
        ///     Цвет линии
        /// </summary>
        [DisplayName(@"Цвет линии")]
        [Category("Свойства фигуры")]
        public Color PenColor { get; set; }
        /// <summary>
        ///     Цвет заливки
        /// </summary>
        [DisplayName(@"Цвет заливки")]
        [Category("Свойства фигуры")]
        public Color FillColor { get; set; }
        /// <summary>
        ///     Тип кисти
        /// </summary>
        [DisplayName(@"Тип кисти")]
        [Category("Свойства фигуры")]
        public BrushType BrushType { get; set; }

        public ShapeProperties(Color penColor, Color fillColor, BrushType brushType, int brushHeight)
        {
            PenColor = penColor;
            FillColor = fillColor;
            BrushType = brushType;
            BrushHeight = brushHeight;
        }

        public ShapeProperties Clone()
        {
            return CloneImpl();
        }

        object ICloneable.Clone()
        {
            return CloneImpl();
        }

        private ShapeProperties CloneImpl()
        {
            return (ShapeProperties) MemberwiseClone();
        }


    }
}
