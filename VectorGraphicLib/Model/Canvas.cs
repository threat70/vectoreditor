﻿using System.Collections.Generic;
using VectorGraphicLib.Shapes;
using Point = System.Drawing.Point;

namespace VectorGraphicLib.Model
{
    /// <summary>
    ///     Холст для отображения элементов
    /// </summary>
    public class Canvas
    {
        /// <summary>
        ///     Стратегия отрисовки, определяет как и чем отрисовывать фигуры
        /// </summary>
        private readonly IDrawStrategy _drawStrategy;

        /// <summary>
        ///     Фигуры, которые должны быть отрисованы на холсте
        /// </summary>
        private readonly List<Shape> _shapes;

        public Canvas(IDrawStrategy drawer)
        {
            _drawStrategy = drawer;
            _shapes = new List<Shape>();
        }

        /// <summary>
        ///     Перерисовка всех элементов холста
        /// </summary>
        public void Invalidate()
        {
            _drawStrategy.StartDrawing();
            foreach (var shape in _shapes)
            {
                shape.Draw(_drawStrategy);
            }
            _drawStrategy.StopDrawing();
        }

        /// <summary>
        ///     Добавление фигуры на холст
        /// </summary>
        /// <param name="shape">Фигура для добавления</param>
        public void AddShape(Shape shape)
        {
            _shapes.Add(shape);
        }
        
        /// <summary>
        ///     Удаление всех фигур с холста
        /// </summary>
        public void RemoveAllShapes()
        {
            _shapes.Clear();
            _drawStrategy.Clear();
        }
    }
}
