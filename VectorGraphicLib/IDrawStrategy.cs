﻿using VectorGraphicLib.Shapes;

namespace VectorGraphicLib
{
    public interface IDrawStrategy
    {
        /// <summary>
        ///     Метод для старта отрисовки фигур в буффер памяти.
        ///     Отрисовку фигур необходимо производить между методами StartDrawing() StopDrawing()
        /// </summary>
        void StartDrawing();
        /// <summary>
        ///     Метод для остановки отрисовки фигур в буффер памяти.
        ///     Выводит буффер памяти
        ///     Отрисовку фигур необходимо производить между методами StartDrawing() StopDrawing()
        /// </summary>
        void StopDrawing();
        /// <summary>
        ///     Отрисовка круга
        /// </summary>
        /// <param name="circle"></param>
        void Draw(Circle circle);
        /// <summary>
        ///     Отрисовка эллипса
        /// </summary>
        /// <param name="ellipse"></param>
        void Draw(Ellipse ellipse);
        /// <summary>
        ///     Отрисовка линии
        /// </summary>
        /// <param name="line"></param>
        void Draw(Line line);
        /// <summary>
        ///     Отрисовка многоугольника
        /// </summary>
        /// <param name="polygon"></param>
        void Draw(Polygon polygon);
        /// <summary>
        ///     Отрисовка полилинии
        /// </summary>
        /// <param name="polyLine"></param>
        void Draw(PolyLine polyLine);
        /// <summary>
        ///     Очистка холста, Метод должен вызываться вне блоков StartDrawing, StopDrawing
        /// </summary>
        void Clear();
    }
}
