﻿using System.Drawing;
using System.Windows.Forms;
using VectorEditor.Drawer;
using VectorEditor.Drawer.Controller;
using VectorGraphicLib;

namespace VectorEditor
{
    public partial class DrawerForm : Form, IMessageOutput
    {
        private readonly DrawController _drawController;

        public DrawerForm()
        {
            _drawController = new DrawController();
            InitializeComponent();

            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;

            // mouse click to controller
            pictureBox1.MouseClick += _drawController.MouseClick;
            // mouse move to controller
            pictureBox1.MouseMove += _drawController.MouseMove;
            numericUpDown1.Value = pictureBox1.Size.Width;
            numericUpDown2.Value = pictureBox1.Size.Height;
     

            propertyGrid1.SelectedObject = _drawController.ShapeProperties;
            
            // setup draw strategy for controller
            IDrawStrategy drawStrategy = new GraphicsDrawStrategy(pictureBox1);
            _drawController.SetupCanvas(drawStrategy);
            _drawController.RegisterEventListener(this);
        }

        private void radioButton1_CheckedChanged(object sender, System.EventArgs e)
        {
            if (radioButton1.Checked)
                _drawController.SelectShape(ShapeType.Line);
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        private void radioButton2_CheckedChanged(object sender, System.EventArgs e)
        {
            if (radioButton2.Checked)
                _drawController.SelectShape(ShapeType.Circle);
        }

        private void radioButton4_CheckedChanged(object sender, System.EventArgs e)
        {
            if (radioButton4.Checked)
                _drawController.SelectShape(ShapeType.PolyLine);
        }

        private void radioButton3_CheckedChanged(object sender, System.EventArgs e)
        {
            if (radioButton3.Checked)
                _drawController.SelectShape(ShapeType.Ellipse);
        }

        private void radioButton5_CheckedChanged(object sender, System.EventArgs e)
        {
            if (radioButton5.Checked)
                _drawController.SelectShape(ShapeType.Polygon);
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            _drawController.RemoveAllShapes();
        }

        private void numericUpDown2_ValueChanged(object sender, System.EventArgs e)
        {
            UpdateImageSize((int) numericUpDown1.Value, (int) numericUpDown2.Value);
        }

        private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
        {
            UpdateImageSize((int) numericUpDown1.Value, (int) numericUpDown2.Value);
        }

        private void UpdateImageSize(int width, int height)
        {
            panel1.AutoScrollMinSize = new Size(width, height);
            if (width == 0 || height == 0) return;
            
            if (pictureBox1.Image == null)
            {
                pictureBox1.Image = new Bitmap(width, height);
            }
            else
            {
                var newImage = new Bitmap(width, height);
                using (var graphics = Graphics.FromImage(newImage))
                {
                    graphics.DrawImageUnscaled(pictureBox1.Image, new Point(0, 0));
                }

                pictureBox1.Image = newImage;
            }
        }

        private void pictureBox1_LoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            UpdateImageSize(panel1.Size.Width, panel1.Size.Height);
        }
    }
}
