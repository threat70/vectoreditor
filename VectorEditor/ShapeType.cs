﻿namespace VectorEditor
{
    public enum ShapeType
    {
        Line,
        Circle,
        Ellipse,
        Polygon,
        PolyLine
    }
}
