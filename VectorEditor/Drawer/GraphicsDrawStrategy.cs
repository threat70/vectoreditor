﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using VectorGraphicLib;
using VectorGraphicLib.Shapes;
using Point = System.Drawing.Point;

namespace VectorEditor.Drawer
{
    public class GraphicsDrawStrategy : IDrawStrategy
    {
        private readonly PictureBox _pictureBox;
        private Graphics _currentGraphics;
        private Bitmap _currentImage;

        public GraphicsDrawStrategy(PictureBox pictureBox)
        {
            _pictureBox = pictureBox;
        }

        public void StartDrawing()
        {
            _currentImage = new Bitmap(_pictureBox.Size.Width, _pictureBox.Size.Height);
            _currentGraphics = Graphics.FromImage(_currentImage);
            _currentGraphics.InterpolationMode = InterpolationMode.NearestNeighbor;
        }

        public void StopDrawing()
        {
            _pictureBox.Image = (Image) _currentImage.Clone();
            _currentImage = null;
            _currentGraphics.Dispose();
        }

        public void Draw(Circle circle)
        {
            using (var brush = ConfigureFillBrush(circle))
            {
                _currentGraphics.FillEllipse(brush,
                    circle.CenterPoint.X - circle.Radius,
                    circle.CenterPoint.Y - circle.Radius,
                    circle.Radius + circle.Radius,
                    circle.Radius + circle.Radius);
            }
            using (var pen = ConfigurePen(circle))
            {
                _currentGraphics.DrawEllipse(pen,
                    circle.CenterPoint.X - circle.Radius,
                    circle.CenterPoint.Y - circle.Radius,
                    circle.Radius + circle.Radius,
                    circle.Radius + circle.Radius);
            }
        }

        public void Draw(Ellipse ellipse)
        {
            using (var brush = ConfigureFillBrush(ellipse))
            {
                _currentGraphics.FillEllipse(brush,
                    ellipse.StartPoint.X,
                    ellipse.StartPoint.Y,
                    Math.Abs(ellipse.EndPoint.X - ellipse.StartPoint.X),
                    Math.Abs(ellipse.EndPoint.Y - ellipse.StartPoint.Y));
            }
            using (var pen = ConfigurePen(ellipse))
            {
                _currentGraphics.DrawEllipse(pen,
                    ellipse.StartPoint.X,
                    ellipse.StartPoint.Y,
                    Math.Abs(ellipse.EndPoint.X - ellipse.StartPoint.X),
                    Math.Abs(ellipse.EndPoint.Y - ellipse.StartPoint.Y));
            }
        }

        public void Draw(Line line)
        {
            using (var pen = ConfigurePen(line))
            {
                _currentGraphics.DrawLine(
                    pen,
                    ToGraphicsPoint(line.StartPoint),
                    ToGraphicsPoint(line.EndPoint)
                    );
            }
      }

        public void Draw(Polygon polygon)
        {
            using (var brush = ConfigureFillBrush(polygon))
            {
                _currentGraphics.FillPolygon(brush,
                    polygon.Points.ConvertAll(ToGraphicsPoint).ToArray()
                    );
            }
            using (var pen = ConfigurePen(polygon))
            {
                _currentGraphics.DrawPolygon(pen,
                    polygon.Points.ConvertAll(ToGraphicsPoint).ToArray()
                    );
            }
        }

        public void Draw(PolyLine polyLine)
        {
            using (var pen = ConfigurePen(polyLine))
            {
                _currentGraphics.DrawLines(
                    pen,
                    polyLine.Points.ConvertAll(ToGraphicsPoint).ToArray()
                    );
            }
        }

        public void Clear()
        {
            using (var graphics = Graphics.FromImage(_pictureBox.Image))
            {
                graphics.Clear(Color.White);
            }
            _pictureBox.Refresh();
        }

        private static Point ToGraphicsPoint(VectorGraphicLib.Shapes.Point point)
        {
            return new Point(point.X, point.Y);
        }

        private static Pen ConfigurePen(Shape shape)
        {
            var pen = new Pen(shape.PenColor, shape.BrushHeight);
            switch (shape.BrushType)
            {
                case BrushType.Solid:
                    break;
                case BrushType.Dotted:
                    pen.DashStyle = DashStyle.DashDot;
                    break;
            }
            return pen;
        }

        private static Brush ConfigureFillBrush(Shape shape)
        {
            return new SolidBrush(shape.FillColor);
        }
    }
}
