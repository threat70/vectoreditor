﻿
using System;

namespace VectorEditor.Drawer
{
    public interface IMessageOutput
    {
        void ShowMessage(String message);
    }
}
