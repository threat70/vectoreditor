﻿using System.Windows.Forms;

namespace VectorEditor.Drawer
{
    public interface IMouseHandler
    {
        void HandleClick(MouseEventArgs e);
        void HandleMove(MouseEventArgs e);
        bool IsBusy();
    }
}
