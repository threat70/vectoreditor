﻿using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer
{
    public interface ICanvasAdapter
    {
        void Invalidate();
        void AddShape(Shape shape);
    }
}
