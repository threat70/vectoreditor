﻿using System.Windows.Forms;
using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer.Mouse
{
    public abstract class AbstractMouseHandler : IMouseHandler
    {
        /// <summary>
        ///     Адаптер для работы с моделью канваса
        /// </summary>
        protected readonly ICanvasAdapter Canvas;
        
        /// <summary>
        ///     Рисуем ли сейчас фигуру
        /// </summary>
        public bool InDraw { get; protected set; }

        /// <summary>
        ///     Точка, где сейчас находится мышка.
        /// </summary>
        protected Point MousePoint { get; set; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="canvas">Адаптер для работы с канвасом</param>
        protected AbstractMouseHandler(ICanvasAdapter canvas)
        {
            Canvas = canvas;
            MousePoint = new Point(0, 0);
        }

        /// <summary>
        ///     Абстрактный метод для обработки клика
        /// </summary>
        /// <param name="e"></param>
        public abstract void HandleClick(MouseEventArgs e);

        /// <summary>
        ///     Метод для обработки движения мышки, нет необходимости переопределять
        /// </summary>
        /// <param name="e"></param>
        public void HandleMove(MouseEventArgs e)
        {
            if (!InDraw || MousePoint == null) return;
            if (MousePoint.X == e.X && MousePoint.Y == e.Y) return;
            
            MousePoint.X = e.X;
            MousePoint.Y = e.Y;

            Canvas.Invalidate();
        }

        /// <summary>
        ///     Рисуем ли сейчас фигуру
        /// </summary>
        /// <returns>Возвращает свойство InDraw</returns>
        public bool IsBusy()
        {
            return InDraw;
        }
    }
}
