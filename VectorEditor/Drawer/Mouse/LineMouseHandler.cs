﻿using System.Windows.Forms;
using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer.Mouse
{
    public class LineMouseHandler : AbstractMouseHandler
    {
        public LineMouseHandler(ICanvasAdapter canvas) : base(canvas)
        {
            
        }

        /// <summary>
        ///     Обработчик кликов мышкой
        /// </summary>
        /// <param name="e"></param>
        public override void HandleClick(MouseEventArgs e)
        {
            if (!InDraw)
            {
                MousePoint = new Point(e.X, e.Y);
                InDraw = true;

                var line = new Line(new Point(e.X, e.Y), MousePoint);
                Canvas.AddShape(line);
                // invalidate not needed, this called from HandleMove
            }
            else
            {
                InDraw = false;
                MousePoint = null;
            }
        }
    }
}
