﻿using System.Windows.Forms;
using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer.Mouse
{
    public class PolyLineMouseHandler : AbstractMouseHandler
    {
        /// <summary>
        ///     Полилиния, которая сейчас рисуется
        /// </summary>
        private PolyLine _currentPolyLine;

        public PolyLineMouseHandler(ICanvasAdapter canvas) : base(canvas)
        {
        }

        /// <summary>
        ///     Обработчик кликов мышкой
        /// </summary>
        /// <param name="e"></param>
        public override void HandleClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) { 
                if (!InDraw)
                {
                    MousePoint = new Point(e.X, e.Y);
                    InDraw = true;
                    _currentPolyLine = new PolyLine();

                    _currentPolyLine.AddPoint(new Point(e.X, e.Y));
                    _currentPolyLine.AddPoint(MousePoint);
                    Canvas.AddShape(_currentPolyLine);
                    // invalidate not needed, this called from HandleMove
                }
                else
                {
                    MousePoint = new Point(e.X, e.Y);
                    _currentPolyLine.AddPoint(MousePoint);
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                _currentPolyLine = null;
                MousePoint = null;
                InDraw = false;
            }
        }
    }
}
