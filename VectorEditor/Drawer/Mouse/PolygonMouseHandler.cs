﻿using System.Windows.Forms;
using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer.Mouse
{
    public class PolygonMouseHandler : AbstractMouseHandler
    {
        /// <summary>
        ///     Многоугольник, который сейчас рисуется
        /// </summary>
        private Polygon _currentPolygon;

        public PolygonMouseHandler(ICanvasAdapter canvas) : base(canvas)
        {
        }

        /// <summary>
        ///     Обработчик кликов мышкой
        /// </summary>
        /// <param name="e"></param>
        public override void HandleClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!InDraw)
                {
                    MousePoint = new Point(e.X, e.Y);
                    InDraw = true;
                    _currentPolygon = new Polygon();

                    _currentPolygon.AddPoint(new Point(e.X, e.Y));
                    _currentPolygon.AddPoint(MousePoint);
                    Canvas.AddShape(_currentPolygon);
                    // invalidate not needed, this called from HandleMove
                }
                else
                {
                    MousePoint = new Point(e.X, e.Y);
                    _currentPolygon.AddPoint(MousePoint);
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                _currentPolygon = null;
                MousePoint = null;
                InDraw = false;
            }
        }
    }
}
