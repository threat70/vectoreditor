﻿using System.Windows.Forms;
using VectorGraphicLib.Shapes;

namespace VectorEditor.Drawer.Mouse
{
    class CircleMouseHandler : AbstractMouseHandler
    {
        public CircleMouseHandler(ICanvasAdapter canvas) : base(canvas)
        {
        }

        /// <summary>
        ///     Обработчик кликов мышкой
        /// </summary>
        /// <param name="e"></param>
        public override void HandleClick(MouseEventArgs e)
        {
            if (!InDraw)
            {
                MousePoint = new Point(e.X, e.Y);
                InDraw = true;

                var circle = new Circle(new Point(e.X, e.Y), MousePoint);
                Canvas.AddShape(circle);
                // invalidate not needed, this called from HandleMove
            }
            else
            {
                InDraw = false;
                MousePoint = null;
            }
        }
    }
}
