﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using VectorEditor.Drawer.Mouse;
using VectorGraphicLib;
using VectorGraphicLib.Shapes;
using VectorGraphicLib.Model;

namespace VectorEditor.Drawer.Controller
{
    public class DrawController : ICanvasAdapter
    {
        public ShapeProperties ShapeProperties;

        private Canvas _canvas;
        private IMessageOutput _messageOutput;
        private IMouseHandler _currentMouseHandler;
        private readonly Dictionary<ShapeType, IMouseHandler> _mouseHandlers = new Dictionary<ShapeType, IMouseHandler>();

        public DrawController()
        {
            ShapeProperties = new ShapeProperties(Color.Black, Color.White, BrushType.Solid, 1);
            _mouseHandlers.Add(ShapeType.Line, new LineMouseHandler(this));
            _mouseHandlers.Add(ShapeType.Circle, new CircleMouseHandler(this));
            _mouseHandlers.Add(ShapeType.Ellipse, new EllipseMouseHandler(this));
            _mouseHandlers.Add(ShapeType.PolyLine, new PolyLineMouseHandler(this));
            _mouseHandlers.Add(ShapeType.Polygon, new PolygonMouseHandler(this));
        }

        public void SetupCanvas(IDrawStrategy drawer)
        {
            if (_canvas == null)
                _canvas = new Canvas(drawer);
        }

        public bool SelectShape(ShapeType shapeType)
        {
            if (_currentMouseHandler != null && _currentMouseHandler.IsBusy())
            {
                // maybe need replace message
                if (_messageOutput != null)
                    _messageOutput.ShowMessage("Закончите отрисовку фигуры");
                return false;
            }
            return _mouseHandlers.TryGetValue(shapeType, out _currentMouseHandler);
        }

        public void RegisterEventListener(IMessageOutput messageOutput)
        {
            _messageOutput = messageOutput;
        }

        #region Евенты для работы с мышкой

        public void MouseClick(object sender, MouseEventArgs e)
        {
            if (_currentMouseHandler != null)
                _currentMouseHandler.HandleClick(e);
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            if (_currentMouseHandler != null)
                _currentMouseHandler.HandleMove(e);
        }

        #endregion

        #region ICanvasAdapter methods

        public void Invalidate()
        {
            _canvas.Invalidate();
        }

        public void AddShape(Shape shape)
        {
            // insert properties
            shape.ShapeProperties = ShapeProperties.Clone();

            _canvas.AddShape(shape);
        }

        public void RemoveAllShapes()
        {
            _canvas.RemoveAllShapes();
        }

        #endregion
    }
}
